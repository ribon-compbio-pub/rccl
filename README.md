# Reduced Complexity CRISPR Library (RCCL)

A repository for (R) code and data analysis to generate a reduced-complexity CRISPR library based on gene functional annotation weighting. The method is intended to provide a genome-sampling approach to reduce the CRISPR library  size while maintaining and offering an unbiased, hypothesis-free screening assay.

The code is R-based and contained in multiple R scripts in the src directory. There is no installation necessary to use the code; however, there are considerations for the analysis directory, required packages, and use of specfic functions.
- src/rccl_analysis.R 
- src/rccl_utility.R 

A set of public data were downloaded to demonstrate the sub-sampling approach and demonstrate the effects on the results at a geneset level (see below). 

#### Sampling Method

The sampling method is based on some core concepts around (CRISPR) screening outputs:

1. Screening projects do not need to comprehensively characterize the biology of interest with the screening results, but rather identify new hypotheses to validate and follow-up with additional screens.

2. Analysis workflows from screening results typically apply an enrichment-based analysis to characterize the biology for the screening hits. Sampling the genome based on the enrichment resources provides a straightforward and direct connection to the analysis process and output.

In short, the method quantifies the amount to which any given gene in the genome contributes to the overall annotation information. The quanitified annotation value can then be used to rank order the genes and sampling would select the top percentage of genes for use. The quantification approach is designed to up-weight genes that contribute more unique or valuable information to a particular gene set, which is evaluated using the number of genes in a given annotated gene set. Genes in a large gene set would be down-weighted since the gene set annotation information is captured and spread across many genes. 

This approach is flexible for 1) making use of any gene-annotation source whereby the genes are assigned to one or more annotation value and 2) allowing the user to define the gene sampling amount.

#### Genome Sub-sampling Analyses

To demonstrate the annotation-based sampling, the Entrez gene protein-coding genome can be scored and sampled and a set of public CRISPR datasets will be analyzed. Lastly, a generic function, rccl_sampling(), is available in the rccl_utility.R to sample a full library using a user-provided set of gene-annotations.

##### Entrez Gene Annotation Weighting

A simple demonstration is the scoring of the Entrez Gene protein coding human genome. The genes can be obtained via the [ftp site](ftp://ftp.ncbi.nlm.nih.gov/gene/DATA/). The get_entrez_genes() function in the rccl_utility.R script can also be used to format the data into a data frame.

```
# Clone directory and set as analysis directory
git clone git@gitlab.com:ribon-compbio-pub/rccl.git
cd rccl
R

# R code to obtain Entrez genes
source("src/rccl_utility.R")
param.list <- list(base.dir = getwd(), update.annotations = FALSE)
entrez.df <- get_entrez_genes(param.list)

#   GeneID Gene.symbol                 Gene.name      Gene.type
# 1      1        A1BG    alpha-1-B glycoprotein protein-coding
# 2      2         A2M     alpha-2-macroglobulin protein-coding
# 3      9        NAT1     N-acetyltransferase 1 protein-coding
# 4     10        NAT2     N-acetyltransferase 2 protein-coding
# 5     12    SERPINA3  serpin family A member 3 protein-coding
# 6     13       AADAC arylacetamide deacetylase protein-coding
head(entrez.df)
```

To score the genes, four different annotation sources were considered: KEGG, Reactome, Gene Ontology, and Molecular Signature Database Curated Pathways. The get_annotations() function in the rccl_utility.R script is designed to obtain the annotations from their source and format the data. The get_annotation_weights() function in the rccl_utility.R script will use the formatted annotation data to then weight the Entrez gene protein-coding genome and return a data frame with the weighted values. 

```
# Clone directory and set as analysis directory
git clone git@gitlab.com:ribon-compbio-pub/rccl.git
cd rccl
R

# R code to obtain Entrez genes
source("src/rccl_utility.R")
param.list <- list(base.dir = getwd(), update.annotations = FALSE)
weights.df <- get_annotation_weights(param.list)

#     GeneID Gene.symbol                                        Gene.name      Gene.type  Reactome        KEGG MSigDB.Hallmark  MSigDB.C2 GO.Component GO.Function GO.Process total.norm.sum idx
# 3775   5594       MAPK1               mitogen-activated protein kinase 1 protein-coding 3.8932118 1.063520399      0.01994048 3.69455227  0.182237957   0.3164766  4.6874379       1.052417   1
# 1940   2885        GRB2           growth factor receptor bound protein 2 protein-coding 5.9988557 0.401938138      0.02601806 3.06293471  0.575375441   0.3917405  0.9497063       1.051253   2
# 3776   5595       MAPK3               mitogen-activated protein kinase 3 protein-coding 3.1803041 1.063520399      0.00000000 4.07587938  0.100007084   0.1948909  5.4010478       1.029006   3
# 448     652        BMP4                     bone morphogenetic protein 4 protein-coding 0.0703112 0.061029883      0.00000000 0.05604431  0.004420539   0.2699170 37.1248284       1.019746   4
# 4599   6714         SRC SRC proto-oncogene, non-receptor tyrosine kinase protein-coding 3.9633482 0.327134149      0.01000000 2.55003417  0.367625793   0.8610856  9.7550410       1.015576   5
# 7768  11315       PARK7                Parkinsonism associated deglycase protein-coding 0.1591501 0.007042254      0.00000000 0.03030303  0.081501436   5.9592488 30.9650643       1.010188   6
head(weights.df)
```

##### CRISPR Dataset Sub-sampling Analyses

The CRISPR results from six different publications were downloaded and formatted. Most of the datasets used a fold-change based metric to report a gene CRISPR effect, but any continuous variable that provided a gene rank order was used in the analysis. 

###### Datasets (see data/external/CRISPR_datasets)
- PARP1 inhibitor synthetic lethality CRISPR screen in cancer cell lines [(Zimmermann et al., 2018)](https://www.ncbi.nlm.nih.gov/pubmed/29973717)
- Rigosertib CRISPRi/a-Based Chemical Genetic Screens [Jost et al., 2017](https://www.ncbi.nlm.nih.gov/pubmed/28985505)
- CRISPR deletion screens in the setting of BRAF, MEK, EGFR, and ALK inhibition [Krall et al., 2017](https://www.ncbi.nlm.nih.gov/pubmed/28145866)
- ATR inhibitor synthetic lethality CRISPR screen in cell lines [Wang et al., 2019](https://www.ncbi.nlm.nih.gov/pubmed/30532030)
- Genome-wide CRISPR screens with RNF43-mutant pancreatic cells [Steinhart et al., 2017](https://www.ncbi.nlm.nih.gov/pubmed/27869803)
- CRISPR dropout screen in AML cells [Tzelepis et al., 2016](https://www.ncbi.nlm.nih.gov/pubmed/27760321)

```
git clone git@gitlab.com:ribon-compbio-pub/rccl.git
cd rccl

# Run R script with rccl as analysis directory and update annotations set to 
# TRUE. This will analyze all the datasets using various sampling methods, 
# including the annotation-based approach, random sampling, and using the
# druggable genome.
Rscript src/rccl_analysis.R $PWD TRUE
```

###### Scoring Genome Library

To sample a generic set of genes (i.e., a CRISPR library) using a set of annotations (i.e., KEGG, Reactome, GO), the rccl_sampling() function in the rccl_utility.R script will perform the weighting and sampling based on a user-specific genome fraction to sample. The code below demonstrates the sampling using the Entrez gene protein-coding genome with the KEGG and Reactome annotation resources.

```
git clone git@gitlab.com:ribon-compbio-pub/rccl.git
cd rccl
R

# R code
source("src/rccl_utility.R")
library.df <- get_entrez_genes(param.list)
annotation.list <- list(kegg = get_kegg(param.list), 
                        reactome = get_reactome(param.list))
rccl.df <- rccl_sampling(library.df, annotation.list, 0.8)
```